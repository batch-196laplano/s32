let http = require("http");

let courses = [
	{
		name: "Phyton 101",
		description : "Learn Phyton",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description : "Learn ReactJS",
		price: 35000
	},
	{
		name: "ExpressJS 101",
		description : "Learn EpressJS",
		price: 28000
	}
];

 http.createServer(function(request,response){
 	
 	console.log(request.url);
 	//get method
 	console.log(request.method);

 	if(request.url==="/" && request.method === "GET"){
 		response.writeHead(200,{'Content-Type':'text/plain'});
 		response.end("This is the / endpoint. GET method request");
 	}else if(request.url ==="/" && request.method === "POST"){
 		response.writeHead(200,{'Content-Type':'text/plain'});
 		response.end("This is the / endpoint. POST method request");
 	}else if (request.url === "/" && request.method === "PUT"){
 		response.writeHead(200,{'Content-Type':'text/plain'});
 		response.end("This is the / endpoint. PUT method request");
 	}else if (request.url === "/" && request.method === "DELETE"){
 		response.writeHead(200,{'Content-Type':'text/plain'});
 		response.end("This is the / endpoint. DELETE method request");
 	}else if (request.url === "/courses" && request.method === "GET"){
 		response.writeHead(200,{'Content-Type':'application/json'});
 		// response.end("This is a response to a GET method request for the /courses");
 		response.end(JSON.stringify(courses)); 
 		//must be JSON.stringify

 	}else if (request.url === "/courses" && request.method === "POST"){


 		//postman > body > raw> JSON

 		let requestBody = "";
 		// step1 data step saved in requestBody
 		request.on('data',function(data){
 			//console.log(data);
 			requestBody+=data;
 		})
 		// step2 end step
 		request.on('end',function(){
 			// console.log(requestBody);
 			// convert json to string
 			// parse and push
 			requestBody = JSON.parse(requestBody);
 			//console.log(requestBody); // now it is an object
 			// push to the array
 			courses.push(requestBody);
 			//console.log(courses);
 			response.writeHead(200,{'Content-Type':'application/json'});
 			response.end(JSON.stringify(courses));
 		})


 	}
 }).listen(4000)
 console.log("Server running at localhost:4000")